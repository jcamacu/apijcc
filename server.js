//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyparser = require('body-parser');
var bcrypt = require('bcrypt');
var localStorage = require('localStorage')

var BCRYPT_SALT_ROUNDS = 12;

var requestjson = require('request-json');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/jcamarena/collections"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"

var clienteMlabRaiz;

var urlMovimientosMlab = " https://api.mlab.com/api/1/databases/jcamarena/collections/JCCMovimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlUsuariosMlab = " https://api.mlab.com/api/1/databases/jcamarena/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";


var clienteMlab = requestjson.createClient(urlMovimientosMlab);
var usuarioMlab = requestjson.createClient(urlUsuariosMlab);

var path = require('path');

app.use(bodyparser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type. Accept");
  next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function (req, res) {
  res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/',function (req, res) {
  res.send("Hemos recibido su peticion post")
})

app.post('/JCCMovimientos',function (req, res) {
  clienteMlab.post('', req.body, function(err, res, body) {
    if (err){
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/api/v1/login',function (req, res) {
  var email = req.headers.email
  var password = req.headers.password

  var query = 'q={"email":"'+ email + '","password": "' + password +'"}'

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCUsuarios?" + apiKey + "&" + query)

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err){
      if (body.length == 1){
        res.status(200).send('Usuario logado');
      } else {
        res.status(404).send('Usuario no encontrado');
      }
    }
  })
})

app.post('/api/v1/login2',function (req, res) {
  var email = req.headers.email
  var password = req.headers.password
  var usuario = {"email":email};

  var query = 'q={"email":"'+ email + '"}'

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCUsuarios?" + apiKey + "&" + query)

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err){
      if (body.length == 1){
          var hash = body[0]
          bcrypt.compare(password, hash.password, function(err, res2) {
          if(res2) {
            res.status(200).send('Usuario logado');
          } else {
            res.status(404).send('Usuario no encontrado');
          }
        })
      } else {
          res.status(404).send('Usuario no encontrado');
      }
    }
  })
});


app.post('/api/v1/recupera',function (req, res) {
  var email = req.headers.email

  var query = 'q={"email":"'+ email +'"}'

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCUsuarios?" + apiKey + "&" + query)
  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err){
      if (body.length == 1){
        res.status(200).send('Usuario logado');
      } else {
        res.status(404).send('Usuario no encontrado');
      }
    }
  })
});

app.get('/api/v1/urecupera',function (req, res) {
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCUsuarios?" + apiKey )

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err){
      if (body.length == 0){
        res.status(500).send('No hay Usuario registrados');
      } else {
        res.status(200).send(body);
      }
    }
  })
});

app.get('/api/v1/crecupera',function (req, res) {
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCCuentas?" + apiKey )

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err){
      if (body.length == 0){
        res.status(500).send('No hay Cuentas registrados');
      } else {
        res.status(200).send(body);
      }
    }
  })
});

app.get('/api/v1/mrecupera',function (req, res) {
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCMovimientos?" + apiKey )

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err){
      if (body.length == 0){
        res.status(500).send('No hay Movimientos registrados');
      } else {
        res.status(200).send(body);
      }
    }
  })
});

app.post('/api/v1/registra',function (req, res) {
  var nombre = req.headers.nombre
  var apellido = req.headers.apellido
  var email = req.headers.email
  var password = req.headers.password
  var idcon = 0

  let hash = bcrypt.hashSync(password, 10);

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCUsuarios?" + apiKey)

  clienteMlabRaiz.get('', function(err, resM, body) {
    idcon = body.length + 1;
    var data = {
      idcon: idcon,
      nombre: nombre,
      apellido: apellido,
      email: email,
      password: hash
    };

    clienteMlabRaiz.post('', data, function(err, resM, body) {
      if (!err){
          res.status(200).send('Usuario registrado');
        } else {
          res.status(404).send('Usuario no registrado');
        }
    })
  })
});

app.post('/api/v1/cguardar',function (req, res) {
  var cliente = req.headers.cliente
  var cuenta = req.headers.cuenta
  var tipo = req.headers.tipo
  var descripcion = req.headers.descripcion
  var email = req.headers.email

  var data = {
    cliente: cliente,
    cuenta: cuenta,
    tipo: tipo,
    descripcion: descripcion,
    usuario: email,
    fecharegistro: hoyFecha()
  };

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCCuentas?" + apiKey)
  clienteMlabRaiz.post('', data, function(err, resM, body) {
    if (!err){
        res.status(200).send('Cliente Registrado');
      } else {
        res.status(404).send('No se pudo registrar el cliente');
    }
  })
});

app.post('/api/v1/mguardar',function (req, res) {
  var cliente = req.headers.cliente
  var cuenta = req.headers.cuenta
  var tipo = req.headers.tipo
  var importe = req.headers.importe
  var fecha = req.headers.fecha
  var descripcion = req.headers.descripcion
  var email = req.headers.email
  var idmov = 0;

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/JCCMovimientos?" + apiKey)

  clienteMlabRaiz.get('', function(err, resM, body) {
      var idmov = body.length + 1;
      var data = {
        cliente: cliente,
        cuenta: cuenta,
        idmov: idmov,
        fecha: fecha,
        importe: importe,
        tipo: tipo,
        usuario: email,
        fecharegistro: hoyFecha()
      };

      clienteMlabRaiz.post('', data, function(err, resM, body) {
        if (!err){
          res.status(200).send('Movimiento Registrado');
        } else {
          res.status(404).send('No se pudo registrar el movimiento');
        }
      })
  })
});

function hoyFecha() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yyyy = hoy.getFullYear();

    dd = addZero(dd);
    mm = addZero(mm);

    return dd+'/'+mm+'/'+yyyy;
};

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
};
